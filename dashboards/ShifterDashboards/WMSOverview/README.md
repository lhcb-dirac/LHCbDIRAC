# WMS Overview
The `WMS Overview` dashboard can be found in the [Shifter Overview](https://monit-grafana.cern.ch/d/vBnTR3AVz/wms-overview?orgId=46) folder of the LHCb grafana organisation.
The dashboard is organised by six 'rows' that each represent a grouping key (`JobSplitType`, `JobGroup`, `Site`, `UserGroup`, `User`, `UserGroup: lhcb_user`)

For each row/grouping key, five plots are made. Each plot shows the number of jobs with a `Status` corresponding to one of the statuses listed below.
- Checking
- Waiting
- Running
- Completed
- Staging


A variable is used to repeat the base plots for all the different grouping keys.

The datasource for all these plots is WMS indices in elasticsearch.
Note: These plots are not yet validated ([issue](https://github.com/DIRACGrid/WebAppDIRAC/issues/722) blocking validation)
